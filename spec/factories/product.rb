FactoryBot.define do
  factory :product_with_image, parent: :base_product do
    after :create do |product|
      product.images << create(:image)
    end
  end
end
