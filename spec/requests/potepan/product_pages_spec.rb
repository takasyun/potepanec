require 'rails_helper'

RSpec.describe "ProductPages", type: :request do
  describe "Productページ確認テスト" do
    subject { response.body }

    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product_with_image, taxons: [taxon]) }
    let!(:products) { create_list(:product, 5, taxons: [taxon]) }
    let!(:noimage_product) { create(:product, taxons: [taxon]) }
    let!(:notaxon_product) { create(:product) }

    before do
      get potepan_product_path(product.id)
    end

    it "Productページアクセス確認" do
      expect(response).to have_http_status(200)
    end

    it "商品名が表示される" do
      is_expected.to include product.name
    end

    it "商品価格が表示される" do
      is_expected.to include product.price.to_s
    end

    it "商品詳細が表示される" do
      is_expected.to include product.description
    end

    it "関連商品が4つ表示される" do
      expect(controller.instance_variable_get(:@related_products).size).to eq 4
      is_expected.to include products[0].name
      is_expected.to include products[1].name
      is_expected.to include products[2].name
      is_expected.to include products[3].name
      is_expected.not_to include products[4].name
    end

    context "商品画像があるとき" do
      it "商品イメージが表示される" do
        expect(product.images).not_to be_blank
        product.images.each do |image|
          is_expected.to include image.attachment(:large)
        end
      end
    end

    context '商品画像がないとき' do
      it "Noimageが表示される" do
        get potepan_product_path(noimage_product.id)
        is_expected.to include "noimage/large"
      end
    end

    context "productがtaxonを持つ場合" do
      it "一覧ページへ戻るが表記される" do
        is_expected.to include "一覧ページへ戻る"
      end
    end

    context "productがtaxonを持たない場合" do
      it "ホームに戻るが表記される" do
        get potepan_product_path(notaxon_product.id)
        is_expected.to include "ホームに戻る"
      end
    end
  end
end
