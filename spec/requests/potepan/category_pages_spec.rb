require 'rails_helper'

RSpec.describe "CategorytPages", type: :request do
  describe "Categoryページ確認テスト" do
    subject { response.body }

    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "Categoryページアクセス確認" do
      expect(response).to have_http_status(200)
    end

    it "カテゴリ名が表示される" do
      is_expected.to include taxon.name
    end

    it "商品名が表示される" do
      is_expected.to include product.name
    end

    it "商品価格が表示される" do
      is_expected.to include product.price.to_s
    end
  end
end
