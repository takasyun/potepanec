require 'rails_helper'
require 'httpclient'
require 'webmock'

RSpec.describe "SubmitKeywords", type: :request do
  describe "GET /suggests" do
    before do
      WebMock.enable!
      WebMock.stub_request(:get, Rails.application.credentials.potepan[:api_url]).with(
        headers: { Authorization: "Bearer #{Rails.application.credentials.potepan[:api_key]}" },
        query: { keyword: 'ruby', max_num: 5 }
      ).to_return(
        body: res_body,
        status: res_status
      )
      get potepan_suggests_path, params: { keyword: keyword }
    end

    context "レスポンスが正常なとき" do
      let(:res_body) { ["ruby01", "ruby02", "ruby03", "ruby04", "ruby05"] }
      let(:res_status) { 200 }
      let(:keyword) { "ruby" }

      it "レスポンスステータスが200になる" do
        expect(response).to have_http_status(200)
      end

      it "正常なデータが返ってきている" do
        expect(JSON.parse(response.body)).to eq ["ruby01", "ruby02", "ruby03", "ruby04", "ruby05"]
      end
    end

    context "レスポンスがサーバーエラーのとき" do
      let(:res_body) { nil }
      let(:res_status) { 500 }
      let(:keyword) { "ruby" }

      it "レスポンスステータスが500になる" do
        expect(response).to have_http_status(500)
      end

      it "エラーが返ってくる" do
        expect(JSON.parse(response.body)['error']).to eq "エラーが発生しました。(#{res_status}Error)"
      end
    end

    context "無効なparamsを送信したとき" do
      let(:res_body) { nil }
      let(:res_status) { 400 }
      let(:keyword) { nil }

      it "エラーが返ってくる" do
        expect(JSON.parse(response.body)['error']).to eq "Keyword cant be blank"
      end
    end
  end
end
