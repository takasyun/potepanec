require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "related_products" do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let!(:other_taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 3, taxons: [taxon]) }
    let!(:not_related_product) { create(:product, taxons: [other_taxon]) }

    it "関連商品が取得できる" do
      expect(product.related_products(3)).to contain_exactly(related_products[0],
                                                             related_products[1],
                                                             related_products[2])
    end

    it "関連しない商品は取得しない" do
      expect(product.related_products(4)).not_to include not_related_product
    end

    it "関連商品が昇順になっている" do
      expect(product.related_products(3)).to eq related_products.sort
    end

    it "関連商品を指定した数だけ取得する" do
      expect(product.related_products(2).size).to eq 2
    end
  end
end
