require 'rails_helper'

RSpec.feature "ProductPages", type: :feature do
  feature "Productページにアクセス" do
    given!(:taxonomy) { create(:taxonomy) }
    given!(:taxon) { create(:taxon, taxonomy: taxonomy) }
    given!(:product) { create(:product, taxons: [taxon]) }
    given!(:related_product) { create(:product, taxons: [taxon]) }

    before do
      visit potepan_product_path(product.id)
    end

    it "アクセスしたら適切な商品と関連商品が表示される" do
      expect(page).to have_content product.name
      expect(page).to have_content product.price.to_s
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.price.to_s
    end

    it "関連商品ページにアクセスする" do
      click_link related_product.name
      expect(current_path).to eq potepan_product_path(related_product.id)
    end

    it "Categoryページにアクセスする" do
      click_link "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(product.taxons.first.id)
    end

    it "HOMEリンクからホームに戻る" do
      click_link "Home"
      expect(current_path).to eq potepan_root_path
    end
  end
end
