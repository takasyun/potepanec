require 'rails_helper'

RSpec.feature "CategoryPages", type: :feature do
  feature "Categoryページにアクセス" do
    given!(:taxonomy) { create(:taxonomy) }
    given!(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    given!(:other_taxon) { create(:taxon, name: "rails-bag", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    given!(:product) { create(:product, taxons: [taxon]) }
    given!(:not_related_product) { create(:product, taxons: [other_taxon]) }

    before do
      visit potepan_category_path(taxon.id)
    end

    it "アクセスしたら適切な商品とカテゴリーが表示される" do
      expect(page).to have_selector 'h2', text: taxon.name
      expect(page).to have_selector 'h5', text: product.name
      expect(page).to have_content product.price.to_s
      expect(page).not_to have_content not_related_product.name
      within(".side-nav") do
        expect(page).to have_link taxon.name
        expect(page).to have_link other_taxon.name
      end
    end

    it "商品をクリックしたら、Productページにアクセスする" do
      click_link product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end

    it "サイドバーから別のカテゴリーページへアクセスする" do
      within first(".panel-body") do
        click_link other_taxon.name
        expect(current_path).to eq potepan_category_path(other_taxon.id)
      end
    end
  end
end
