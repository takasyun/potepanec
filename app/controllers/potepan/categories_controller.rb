class Potepan::CategoriesController < ApplicationController
  def show
    @taxonomies = Spree::Taxon.roots.includes([:children])
    @category = Spree::Taxon.find(params[:id])
    @products = @category.all_products.with_image_and_price
  end
end
