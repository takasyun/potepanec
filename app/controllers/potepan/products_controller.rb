class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCT_MAXIMUM = 4

  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products(RELATED_PRODUCT_MAXIMUM).with_image_and_price
  end
end
