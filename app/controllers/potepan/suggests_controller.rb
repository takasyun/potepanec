require './app/controllers/shared/potepan_api'

class Potepan::SuggestsController < ApplicationController
  include PotepanApi

  SUGGEST_MAXIMUM = 5

  def index
    render json: { error: "Keyword cant be blank" }, status: 400 and return if params[:keyword].blank?
    res = submit_keywords(params[:keyword], SUGGEST_MAXIMUM)
    if res.status == 200
      render json: res.body, status: res.status
    else
      logger.error { res.inspect }
      render json: { error: "エラーが発生しました。(#{res.status}Error)" }, status: res.status
    end
  end
end
