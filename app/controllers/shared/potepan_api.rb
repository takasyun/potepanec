require 'httpclient'

module PotepanApi
  def submit_keywords(keyword, suggest_size)
    HTTPClient.new.get(
      Rails.application.credentials.potepan[:api_url],
      { keyword: keyword, max_num: suggest_size },
      { Authorization: "Bearer #{Rails.application.credentials.potepan[:api_key]}" }
    )
  end
end
