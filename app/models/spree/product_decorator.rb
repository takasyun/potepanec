Spree::Product.class_eval do
  scope :with_image_and_price, -> { includes(master: [:default_price, :images]) }

  def related_products(products_size)
    Spree::Product.in_taxons(taxons).
      where.not(id: id).
      distinct.
      order(:id).
      limit(products_size)
  end
end
