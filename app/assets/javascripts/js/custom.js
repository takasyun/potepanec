//============================== header =========================
jQuery(document).ready(function($) {

  const navbar = $('.navbar-main');
  const distance = navbar.offset().top;
  $window = $(window);

  $window.scroll(function() {
    if(($window.scrollTop() >= distance) && ($(".navbar-default").hasClass("navbar-main")))
    {
      navbar.removeClass('navbar-fixed-top').addClass('navbar-fixed-top');
      $("body").addClass("padding-top");
      $(".topBar").css("display","none");
    } else {
      navbar.removeClass('navbar-fixed-top');
      $("body").removeClass("padding-top");
      $(".topBar").css("display","block");
    }
  });
});
//============================== FORM SUGGEST =========================
jQuery(document).ready(function(){
  $('#top-form').autocomplete({
    source: function(req, resp){
      $.ajax({
          url: "/potepan/suggests",
          type: "GET",
          cache: false,
          dataType: "json",
          data: {
          keyword: req.term
        },
          success: function(data){
          resp(data);
        },
          error: function(xhr, ts, err){
            const suggest_error = $.parseJSON(xhr.responseText);
            resp(suggest_error);
        }
      })
    }
  })
});
//============================== ALL DROPDOWN ON HOVER =========================
jQuery(document).ready(function(){
  $('.searchBox, .ui-autocomplete').on({
    'mouseenter': function() {
      $('.searchBox').addClass('open');
    },
    'mouseleave': function() {
      $('.searchBox').removeClass('open');
    }
  });
});

jQuery(document).ready(function(){
  $('#top-form').change(function(){
    $('.searchBox').addclass('open');
  });
});

//============================== RS-SLIDER =========================
jQuery(document).ready(function() {
  jQuery('.fullscreenbanner').revolution({
    delay: 5000,
    startwidth: 1170,
    startheight: 500,
    fullWidth: "on",
    fullScreen: "off",
    hideCaptionAtLimit: "",
    dottedOverlay: "twoxtwo",
    navigationStyle: "preview4",
    fullScreenOffsetContainer: "",
    hideTimerBar:"on",
  });
});

//============================== OWL-CAROUSEL =========================
jQuery(document).ready(function() {
  const product_owl = $('.owl-carousel.featuredProductsSlider');
    product_owl.owlCarousel({
      loop:true,
      margin:28,
      autoplay:true,
      autoplayTimeout:2000,
      autoplayHoverPause:true,
      nav:true,
      moveSlides: 4,
      dots: false,
      responsive:{
        320:{
          items:1
        },
        768:{
          items:3
        },
        992:{
          items:4
        }
      }
    });
  const logo_owl = $('.owl-carousel.partnersLogoSlider');
    logo_owl.owlCarousel({
      loop:true,
      margin:28,
      autoplay:true,
      autoplayTimeout:6000,
      autoplayHoverPause:true,
      nav:true,
      dots: false,
      responsive:{
        320:{
          slideBy: 1,
          items:1
        },
        768:{
          slideBy: 3,
          items:3
        },
        992:{
          slideBy: 5,
          items:5
        }
      }
    });
});
//============================== SELECT BOX =========================
jQuery(document).ready(function() {
  $('.select-drop').selectbox();
});

//============================== SIDE NAV MENU TOGGLE =========================
jQuery(document).ready(function() {
  $('.side-nav li a').click(function() {
    $(this).find('i').toggleClass('fa fa-minus fa fa-plus');
  });
});

//============================== PRICE SLIDER RANGER =========================
jQuery(document).ready(function() {
  const minimum = 2000;
  const maximum = 30000;

  $( "#price-range" ).slider({
    range: true,
    min: minimum,
    max: maximum,
    values: [ minimum, maximum ],
    slide: function( event, ui ) {
      $( "#price-amount-1" ).val( ui.values[ 0 ] + "円" );
      $( "#price-amount-2" ).val( ui.values[ 1 ] + "円" );
    }
  });

  $( "#price-amount-1" ).val( $( "#price-range" ) .slider( "values", 0 ) + "円");
  $( "#price-amount-2" ).val( $( "#price-range" ) .slider( "values", 1 ) + "円");
});
//============================== PRODUCT SINGLE SLIDER =========================
jQuery(document).ready(function() {
  (function($){
    $('#thumbcarousel').carousel(0);
    const $thumbItems = $('#thumbcarousel .item');
      $('#carousel').on('slide.bs.carousel', function (event) {
      const $slide = $(event.relatedTarget);
      const thumbIndex = $slide.data('thumb');
      const curThumbIndex = $thumbItems.index($thumbItems.filter('.active').get(0));
      if (curThumbIndex>thumbIndex) {
        $('#thumbcarousel').one('slid.bs.carousel', function (event) {
          $('#thumbcarousel').carousel(thumbIndex);
        });
        if (curThumbIndex === ($thumbItems.length-1)) {
          $('#thumbcarousel').carousel('next');
        } else {
          $('#thumbcarousel').carousel(numThumbItems-1);
        }
      } else {
        $('#thumbcarousel').carousel(thumbIndex);
      }
    });
  })(jQuery);
});

jQuery(document).ready(function() {
  $(".quick-view .btn-block").click(function(){
      $(".quick-view").modal("hide");
    });
});
function FormSubmit() {
  const target = document.getElementById("form01");
  target.method = "post";
  target.submit();
};


//============================== FOOTER COPYRIGHT =========================

// コンテンツが少ない時にfooterをbottom:0にする footerFixed.js
new function(){

  const footerId = "footer";
  //メイン
  function footerFixed(){
    //ドキュメントの高さ
    const dh = document.getElementsByTagName("body")[0].clientHeight;
    //フッターのtopからの位置
    document.getElementById(footerId).style.top = "0px";
    const ft = document.getElementById(footerId).offsetTop;
    //フッターの高さ
    const fh = document.getElementById(footerId).offsetHeight;
    //ウィンドウの高さ
    if (window.innerHeight){
      const wh = window.innerHeight;
    }else if(document.documentElement && document.documentElement.clientHeight != 0){
      const wh = document.documentElement.clientHeight;
    }
    if(ft+fh<wh){
      document.getElementById(footerId).style.position = "relative";
      document.getElementById(footerId).style.top = (wh-fh-ft-1)+"px";
    }
  }

  //文字サイズ
  function checkFontSize(func){

    //判定要素の追加
    const e = document.createElement("div");
    const s = document.createTextNode("S");
    e.appendChild(s);
    e.style.visibility="hidden"
    e.style.position="absolute"
    e.style.top="0"
    document.body.appendChild(e);
    const defHeight = e.offsetHeight;

    //判定関数
    function checkBoxSize(){
      if(defHeight != e.offsetHeight){
        func();
        defHeight= e.offsetHeight;
      }
    }
    setInterval(checkBoxSize,1000)
  }

  //イベントリスナー
  function addEvent(elm,listener,fn){
    try{
      elm.addEventListener(listener,fn,false);
    }catch(e){
      elm.attachEvent("on"+listener,fn);
    }
  }

  addEvent(window,"load",footerFixed);
  addEvent(window,"load",function(){
    checkFontSize(footerFixed);
  });
  addEvent(window,"resize",footerFixed);

}
